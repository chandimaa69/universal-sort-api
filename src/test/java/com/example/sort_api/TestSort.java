package com.example.sort_api;

import org.junit.Assert;
import org.junit.Test;

public class TestSort {

    @Test
    public void testCase1() {
        UniversalSorter<Integer> integerSorter = new UniversalSorter<>();
        Integer[] result1 = integerSorter.sort(Algorithm.BUBBLE_SORT, 14, 34, 25, 12, 22, 11, 90, 0, 200);
        Assert.assertEquals(new Integer[]{0, 11, 12, 14, 22, 25, 34, 90, 200}, result1);
    }

    @Test
    public void testCase2() {
        UniversalSorter<Double> doubleSorter = new UniversalSorter<>();
        Double[] result2 = doubleSorter.sort(Algorithm.SELECTION_SORT, 1.2, 12.3, 2.1, 3.0, 0.0, -0.7);

        Assert.assertEquals(new Double[]{-0.7, 0.0, 1.2, 2.1, 3.0, 12.3}, result2);
    }

    @Test
    public void testCase3() {
        UniversalSorter<String> stringSorter = new UniversalSorter<>();
        String[] result4 = stringSorter.sort(Algorithm.SELECTION_SORT, "abs", "nima", "as", "lakshan", "bsd", "asa", "efec");

        Assert.assertEquals(new String[]{"abs", "as", "asa", "bsd", "efec", "lakshan", "nima"}, result4);
    }

    @Test
    public void testCase4() {
        UniversalSorter<Float> floatSorter = new UniversalSorter<>();
        Float[] result3 = floatSorter.sort(Algorithm.SELECTION_SORT, 1f, 1.3f, 0f, -275f, 4.456f, 4.546f);

        Assert.assertEquals(new Float[]{-275f, 0f, 1f, 1.3f, 4.456f, 4.546f}, result3);
    }
}
