package com.example.sort_api;


import java.util.Arrays;

public class UniversalSorter<E> extends AbstractUniversalSorter<E> {

    public static void main(String[] args) {

        UniversalSorter<Integer> integerSorter = new UniversalSorter<>();
        Integer[] result1 = integerSorter.sort(Algorithm.BUBBLE_SORT, 14, 34, 25, 12, 22, 11, 90, 0, 200);

        UniversalSorter<Double> doubleSorter = new UniversalSorter<>();
        Double[] result2 = doubleSorter.sort(Algorithm.SELECTION_SORT, 1.2, 12.3, 2.1, 3.0, 0.0, -0.7);

        UniversalSorter<Float> floatSorter = new UniversalSorter<>();
        Float[] result3 = floatSorter.sort(Algorithm.SELECTION_SORT, 1f, 1.3f, 0f, -275f, 4.456f, 4.546f);

        UniversalSorter<String> stringSorter = new UniversalSorter<>();
        String[] result4 = stringSorter.sort(Algorithm.SELECTION_SORT, "abs", "nima", "as", "lakshan", "bsd", "asa", "efec");


        System.out.println("result 1 ---> " + Arrays.toString(result1));
        System.out.println("result 2 ---> " + Arrays.toString(result2));
        System.out.println("result 3 ---> " + Arrays.toString(result3));
        System.out.println("result 4 ---> " + Arrays.toString(result4));
        System.out.println();
    }
    
    @SafeVarargs
    @Override
    public final E[] sort(Algorithm algorithm, E... array) {
        return super.sort(algorithm, array);
    }
}
