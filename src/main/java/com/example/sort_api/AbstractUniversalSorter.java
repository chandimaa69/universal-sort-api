package com.example.sort_api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public abstract class AbstractUniversalSorter<E> implements IUniversalSorter<E> {

    private static Logger logger = LogManager.getLogger(AbstractUniversalSorter.class);

    @Override
    public E[] sort(Algorithm algorithm, E... array) {

        logger.info("Start sorting method");

        final int length = array.length;

        final boolean isInteger = array instanceof Integer[];
        final boolean isDouble = array instanceof Double[];
        final boolean isFloat = array instanceof Float[];
        final boolean isString = array instanceof String[];
        final boolean isBoolean = array instanceof Boolean[];

        if (algorithm == Algorithm.BUBBLE_SORT) {

            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length - i - 1; j++) {

                    final E tmp = array[j];

                    if (isInteger) {

                        if (((Integer) tmp) > ((Integer) array[j + 1])) {
                            array[j] = array[j + 1];
                            array[j + 1] = tmp;
                        }

                    } else if (isDouble) {

                        if (((Double) tmp) > ((Double) array[j + 1])) {
                            array[j] = array[j + 1];
                            array[j + 1] = tmp;
                        }

                    } else if (isFloat) {

                        if (((Float) tmp) > ((Float) array[j + 1])) {
                            array[j] = array[j + 1];
                            array[j + 1] = tmp;
                        }

                    } else if (isString) {

                        if (((String) array[j]).compareTo((String) tmp) < 0) {
                            array[i] = array[j];
                            array[j] = tmp;
                        }

                    } else if (isBoolean) {

                    }
                }

            }

        } else if (algorithm == Algorithm.SELECTION_SORT) {

            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {

                    final E tmp = array[i];

                    if (isInteger) {

                        if (((Integer) tmp) < ((Integer) array[j])) {
                            array[i] = array[j];
                            array[j] = tmp;
                        }

                    } else if (isDouble) {

                        if (((Double) tmp) < ((Double) array[j])) {
                            array[i] = array[j];
                            array[j] = tmp;
                        }

                    } else if (isFloat) {

                        if (((Float) tmp) < ((Float) array[j])) {
                            array[i] = array[j];
                            array[j] = tmp;
                        }

                    } else if (isString) {

                        if (((String) tmp).compareTo(((String) array[j])) < 0) {
                            array[i] = array[j];
                            array[j] = tmp;
                        }

                    } else if (isBoolean) {

                    }
                }
            }
        }

        logger.info("Sorted array >> " + Arrays.toString(array));
        logger.info("End sorting method");
        return array;
    }

}
