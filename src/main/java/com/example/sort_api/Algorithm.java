package com.example.sort_api;

public enum  Algorithm {

    BUBBLE_SORT, SELECTION_SORT
}
