package com.example.sort_api;

public interface IUniversalSorter<E> {

    E[] sort(Algorithm algorithm, E... array);
}
