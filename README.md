Industry best practices assignment 1 of 2:

Your task is to create a reusable `Input Soring` framework (Develop your classes + a Main class to demonstrate the usage of your sorter).
* No Rest-APIs, Controllers, endpoints or GUI needed. You’ll just need core JAVA here.
But use `gradle` build system for this Java SE application.

What you’ll need:

1. Good knowledge of Java Generics. Java Generics help develop extremely reusable code. Follow these tutorials with a test project and practice it. It is very easy though it seems difficult.
https://www.tutorialspoint.com/java/java_generics
https://riptutorial.com/java/example/12864/use-of-instanceof-with-generics (usage of `instanceOf` to identify types)

2. Java Auto-boxing. Unboxing, Type-casting and Type-conversion. These techniques are life savers.
https://www.geeksforgeeks.org/autoboxing-unboxing-java/ (Autoboxing, Unboxing)
https://www.studytonight.com/java/type-casting-in-java (Java type casting)
https://dev.to/attacomsian/data-type-conversions-in-java-5ag6 (Java type conversion)

3. Logging with Java (https://www.geeksforgeeks.org/logging-in-java/). You can use log4j library or Java’s inbuilt Logging.

4. Java  :)


The requirement: 

1. In simple terms, your Sorting method should accept an unlimited amount (use Varargs) of following type of values: Integers, Doubles, Floats, Booleans, Strings, Characters… at a time.
(Use `instanceOf` to identify your input type, 
VarArgs: https://stackoverflow.com/questions/3158730/java-3-dots-in-parameters ).

2. The sorting method will support 2 separate basic sorting algorithms. (Choose any 2 sorting algorithms you like. e.g: Bubble Sort, Selection Sort). You need to develop these algorithms. Please refer external sources if needed. These are very simple algorithms.
Use Abstraction (Abstract classes), interfaces where necessary.

3. User of your method should be able to specify which algorithm to use for sorting. (Use `Enum` parameter. e.g: Algorithm.BUBBLE_SORT,  Algorithm.SELECTION_SORT).

4. Your soring method will sort the input list and return.

5. You need to use Java logging to log necessary information while sorting happens.
 (e.g: log what algorithm is chosen before proceed, log start timestamp, log end timestamp, log time taken to sort – You can measure performance of Sorting algorithms this way).

* You can use Java Generics and Autoboxing, Unboxing, Casting, Converstion and any of the Core Java techniques.


Hints:

The `Input Sorting` framework can have a UniversalSorter class (I.e: UniversalSorter.java) which would include a method as following:

```java
public enum Algorithm{
	BUBBLE_SORT,
	SELECTION_SORT
	
}

public class UniversalSorter<E> {
	public E[] sort(Algorithm algo, E... inputs){
	}
}


Usage:

public class Test{
	public static void Main(String[] args) {
		UniversalSorter<Integer> intSorter = new UniversalSorter<>();
		intSorter.sort(Algorithm.BUBBLE_SORT, 2, 40, 1, 0, 10);

		UniversalSorter<String> strSorter = new UniversalSorter<>();
		strSorter.sort(Algorithm.BUBBLE_SORT, “AB”, “ABC”, “BC”, “ACB”);
		strSorter.sort(Algorithm.SELECTION_SORT, “AB”, “ABC”, “BC”, “ACB”);
	}
}
```


